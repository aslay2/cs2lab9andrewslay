package edu.westga.cs1302.techstore.resources;

/**
 * This class keeps track of a number of Strings designed to be standard output
 * messages for the user
 * 
 * @author CS1302
 * @version Fall 2021
 *
 */
public class UIMessage {

	public static final String NULL_PRODUCT_ID = "Product Id cannot be null";
	public static final String EMPTY_PRODUCT_ID = "Product Id cannot be empty";
	public static final String NULL_MANUFACTURER = "Manufacturer cannot be null";
	public static final String EMPTY_MANUFACTURER = "Manufacturer cannot be empty";
	public static final String NULL_MODEL = "Model cannot be null";
	public static final String EMPTY_MODEL = "Model cannot be empty";
	public static final String INVALID_NUMBER_ON_HAND = "The number on hand must be greater than or equal to 0";
	public static final String NULL_COMPUTER = "Computer cannot be null";

}
