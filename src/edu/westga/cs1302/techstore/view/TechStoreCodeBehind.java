package edu.westga.cs1302.techstore.view;

import edu.westga.cs1302.techstore.model.Computer;
import edu.westga.cs1302.techstore.viewmodel.TechStoreViewModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.converter.NumberStringConverter;

/**
 * Defines the code behind file for the view in the Tech Store application.
 * 
 * @author started by CS1302
 * @version Fall 2021
 */
public class TechStoreCodeBehind {

	private TechStoreViewModel viewModel;

	@FXML
	private AnchorPane techStoreAnchorPane;

	@FXML
	private Label titleLabel;

	@FXML
	private Label productIdLabel;

	@FXML
	private TextField productIdTextField;

	@FXML
	private Label manufacturerLabel;

	@FXML
	private TextField manufacturerTextField;

	@FXML
	private Label modelLabel;

	@FXML
	private TextField modelTextField;

	@FXML
	private Label numberOnHandLabel;

	@FXML
	private TextField numberOnHandTextField;

	@FXML
	private Button addButton;

	@FXML
	private Button updateButton;

	@FXML
	private Button searchButton;

	@FXML
	private Label messageLabel;

	@FXML
	private ListView<Computer> computerListView;
	
	/**
	 * Sets up the code behind file for use
	 */
	public TechStoreCodeBehind() {
		this.viewModel = new TechStoreViewModel();
	}

	/**
	 * Initializes the JavaFX items
	 */
	@FXML
	public void initialize() {
		this.bindToViewModel();
	}

	/**
	 * Event handler for the add button
	 * 
	 * @param event the event object that is raised
	 */
	@FXML
	public void btnAddOnClick(ActionEvent event) {
		if (!this.productIdTextField.getText().matches("^([A-Za-z][A-Za-z]\\d{3})$")) {
			this.messageLabel.setText("Required ID Format: DDNNN");
			return;
		}
		this.viewModel.addComputer();
		this.messageLabel.setText("");
	}

	/**
	 * Event handler for the search button
	 * 
	 * @param event the event object that is raised
	 */
	@FXML
	public void btnSearchOnClick(ActionEvent event) {
		if (!this.productIdTextField.getText().matches("^([A-Za-z][A-Za-z]\\d{3})$")) {
			this.messageLabel.setText("Required ID Format: DDNNN");
			return;
		}
		this.viewModel.searchForComputer();
		this.messageLabel.setText("");
	}

	/**
	 * Event handler for the update button
	 * 
	 * @param event the event object that is raised
	 */
	@FXML
	public void btnUpdateOnClick(ActionEvent event) {
		if (!this.productIdTextField.getText().matches("^([A-Za-z][A-Za-z]\\d{3})$")) {
			this.messageLabel.setText("Required ID Format: DDNNN");
			return;
		}
		this.viewModel.updateComputer();
		this.messageLabel.setText("");
	}

	private void bindToViewModel() {
		this.productIdTextField.textProperty().bindBidirectional(this.viewModel.getProductIdProperty());
		this.manufacturerTextField.textProperty().bindBidirectional(this.viewModel.getManufacturerProperty());
		this.modelTextField.textProperty().bindBidirectional(this.viewModel.getModelProperty());
		this.numberOnHandTextField.textProperty().bindBidirectional(this.viewModel.getNumberOnHandProperty(),
				new NumberStringConverter());
		this.computerListView.itemsProperty().bind(this.viewModel.listProperty());
	}
}
