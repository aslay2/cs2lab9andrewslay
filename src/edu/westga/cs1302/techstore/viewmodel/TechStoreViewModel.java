package edu.westga.cs1302.techstore.viewmodel;

import edu.westga.cs1302.techstore.model.Computer;
import edu.westga.cs1302.techstore.model.Inventory;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

/**
 * This class defines the ViewModel class for the Tech Store application
 * 
 * @author started by CS1302
 * @version Fall 2021
 */
public class TechStoreViewModel {
	private final StringProperty productIdProperty;
	private final StringProperty manufacturerProperty;
	private final StringProperty modelProperty;
	private final IntegerProperty numberOnHandProperty;
	private final ListProperty<Computer> listProperty;
	private Inventory inventory;

	/**
	 * Sets up the ViewModel for use
	 */
	public TechStoreViewModel() {
		this.productIdProperty = new SimpleStringProperty();
		this.manufacturerProperty = new SimpleStringProperty();
		this.modelProperty = new SimpleStringProperty();
		this.numberOnHandProperty = new SimpleIntegerProperty();
		this.listProperty = new SimpleListProperty<Computer>();
		
		this.inventory = new Inventory();
	}

	/**
	 * Gets the product id property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the product id property
	 */
	public StringProperty getProductIdProperty() {
		return this.productIdProperty;
	}

	/**
	 * Gets the manufacturer property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the manufacturer property
	 */
	public StringProperty getManufacturerProperty() {
		return this.manufacturerProperty;
	}

	/**
	 * Gets the model property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the model property
	 */
	public StringProperty getModelProperty() {
		return this.modelProperty;
	}

	/**
	 * Gets the number on hand property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the number on hand property
	 */
	public IntegerProperty getNumberOnHandProperty() {
		return this.numberOnHandProperty;
	}

	/**
	 * Adds the Computer
	 * 
	 * @precondition none
	 * @postcondition item is added to the collection
	 * @return true, if successful
	 */
	public boolean addComputer() {
		if (!this.productIdProperty.getValue().isEmpty() 
				&& !this.manufacturerProperty.getValue().isEmpty() 
				&& !this.modelProperty.getValue().isEmpty()
				&& this.numberOnHandProperty.getValue() >= 0) {
			Computer computer = new Computer(
					this.productIdProperty.getValue(), this.manufacturerProperty.getValue(),
					this.modelProperty.getValue(), this.numberOnHandProperty.getValue());
			this.reset();
			this.inventory.add(computer);
			this.listProperty.set(FXCollections.observableArrayList(this.inventory.values()));
			System.out.println(FXCollections.observableArrayList(this.inventory.values()));
			return true;
		}
		
		return false;
	}

	private void reset() {
		this.productIdProperty.set("");
		this.manufacturerProperty.set("");
		this.modelProperty.set("");
		this.numberOnHandProperty.set(0);
	}

	/**
	 * Updates the Computer
	 * 
	 * @precondition none
	 * @postcondition item is updated in the collection
	 * @return true, if successful
	 */
	public boolean updateComputer() {
		if (this.inventory.containsKey(this.productIdProperty.getValue())) {
			this.inventory.get(this.productIdProperty.getValue()).setNumberOnHand(this.numberOnHandProperty.getValue());
			this.reset();
			this.listProperty.set(FXCollections.observableArrayList(this.inventory.values()));
			return true;
		}
		this.reset();
		return false;
	}

	/**
	 * Searches for the Computer
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return true, if item is in the collection
	 */
	public boolean searchForComputer() {
		if (!this.productIdProperty.getValue().isEmpty()) {
			if (this.inventory.containsKey(this.productIdProperty.getValue())) {
				this.productIdProperty.set(this.inventory.get(this.productIdProperty.getValue()).getProductId());
				this.manufacturerProperty.set(this.inventory.get(this.productIdProperty.getValue()).getManufacturer());
				this.modelProperty.set((this.inventory.get(this.productIdProperty.getValue()).getModel()));
				this.numberOnHandProperty.set((this.inventory.get(this.productIdProperty.getValue()).getNumberOnHand()));
				return true;
			}
		} else {
			System.out.println("Please use a product ID to search.");
			this.reset();
			return false;
		}
		
		return false;
	}

	/**
	 * Returns the computer list property.
	 * @return the list property
	 */
	public ListProperty<Computer> listProperty() {
		return this.listProperty;
	}
	
}
