package edu.westga.cs1302.techstore.model;

import edu.westga.cs1302.techstore.resources.UIMessage;

/**
 * This class models a computer from a Tech Store
 * 
 * @author CS1302
 * @version Fall 2021
 *
 */
public class Computer {
	private final String manufacturer;
	private final String model;
	private final String productId;
	private int numberOnHand;

	/**
	 * Creates a Computer for use
	 * 
	 * @precondition productId != null && productId != empty
	 * @precondition manufacturer != null && manufacturer != empty
	 * @precondition model != null && model != empty
	 * @precondition numberOnHand >= 0
	 * 
	 * @postcondition getProductId() == productId && getManufacturer() ==
	 *                manufacturer && getModel() == model && getNumberOnHand() ==
	 *                numberOnHand
	 * 
	 * @param productId    the Computer's identifying number
	 * @param manufacturer the Computer's manufacturer
	 * @param model        the Computer's model
	 * @param numberOnHand the number of this computer currently in the store
	 */
	public Computer(String productId, String manufacturer, String model, int numberOnHand) {
		if (productId == null) {
			throw new IllegalArgumentException(UIMessage.NULL_PRODUCT_ID);
		}
		if (productId.length() == 0) {
			throw new IllegalArgumentException(UIMessage.EMPTY_PRODUCT_ID);
		}

		if (manufacturer == null) {
			throw new IllegalArgumentException(UIMessage.NULL_MANUFACTURER);
		}
		if (manufacturer.length() == 0) {
			throw new IllegalArgumentException(UIMessage.EMPTY_MANUFACTURER);
		}

		if (model == null) {
			throw new IllegalArgumentException(UIMessage.NULL_MODEL);
		}
		if (model.length() == 0) {
			throw new IllegalArgumentException(UIMessage.EMPTY_MODEL);
		}

		if (numberOnHand < 0) {
			throw new IllegalArgumentException(UIMessage.INVALID_NUMBER_ON_HAND);
		}
		this.productId = productId;
		this.manufacturer = manufacturer;
		this.model = model;
		this.numberOnHand = numberOnHand;
	}

	/**
	 * Returns this Computer's product id
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return The product id for this computer
	 */
	public String getProductId() {
		return this.productId;
	}

	/**
	 * Returns this Computer's manufacturer
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return The manufacturer for this Computer
	 */
	public String getManufacturer() {
		return this.manufacturer;
	}

	/**
	 * Returns this Computer's model
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the model for this Computer
	 */
	public String getModel() {
		return this.model;
	}

	/**
	 * Returns the number of this Computer in the store
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the number of Computers currently in the store
	 */
	public int getNumberOnHand() {
		return this.numberOnHand;
	}

	/**
	 * Updates the number on hand to be the value specified
	 * 
	 * @precondition numberOnHand >= 0
	 * @postcondition getNumberOnHand() == numberOnHand
	 * @param numberOnHand the new number currently in stock at the store
	 */
	public void setNumberOnHand(int numberOnHand) {
		if (numberOnHand < 0) {
			throw new IllegalArgumentException(UIMessage.INVALID_NUMBER_ON_HAND);
		}
		this.numberOnHand = numberOnHand;
	}

	/**
	 * Returns a String representation of this Computer, including its manufacturer,
	 * model, product id, and number on hand
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return this Computer's description
	 */
	@Override
	public String toString() {
		return this.productId + ": " + this.manufacturer + ", " + this.model + ", " + this.numberOnHand;
	}
}
