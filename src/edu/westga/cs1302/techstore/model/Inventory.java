package edu.westga.cs1302.techstore.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Creates an inventory class.
 * @author Andrew Slay
 * @version CS1302
 * 
 */
public class Inventory implements Collection<Computer> {

	private final Map<String, Computer> computers;
	
	/**
	 * Creates a new Inventory object and initiates the inventory
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public Inventory() {
		this.computers = new HashMap<String, Computer>();	
	}

	/**
	 * Gets the size of the map
	 * @return the size of the hashmap
	 */
	@Override
	public int size() {
		return this.computers.size();
	}

	/**
	 * Checks to see if the hashmap is empty
	 * @return true if empty, false otherwise
	 */
	@Override
	public boolean isEmpty() {
		return this.computers.isEmpty();
	}

	/**
	 * Checks to see if hashmap contains a computer object
	 * @return true if empty, false otherwise
	 */
	@Override
	public boolean contains(Object computer) {
		if (computer == null) {
			throw new NullPointerException("Student cannot be null");
		}
		
		return this.computers.containsValue(computer);
	}

	/**
	 * Iterates over the collection
	 * @return the computer iterator
	 */
	@Override
	public Iterator<Computer> iterator() {
		return this.computers.values().iterator();
	}

	/**
	 * Converts hashmap to array
	 * @return the array of values
	 */
	@Override
	public Object[] toArray() {
		return this.computers.values().toArray();
	}

	/**
	 * converts hashmap to array
	 * @param computers the computers to put in array form
	 * @return the array of values
	 */
	@Override
	public <T> T[] toArray(T[] computers) {
		return this.computers.values().toArray(computers);
	}

	/**
	 * adds a computer object
	 * @precondition computer != null
	 * @postcondition this.computers.size == 
	 * 				  this.computers.size@prev + 1
	 * @param computer the computer to be aded
	 * @return true if added, false otherwise
	 */
	@Override
	public boolean add(Computer computer) {
		if (computer == null) {
			throw new IllegalArgumentException("Computer cannot be null");
		}
		if (this.computers.containsValue(computer)) {
			return false;
		}
		return this.computers.put(computer.getProductId(), computer) == null;
	}

	/**
	 * removes a computer object
	 * @precondition computer != null
	 * @postcondition this.computers.size == 
	 * 				  this.computers.size@prev - 1
	 * @param computer the computer to be removed
	 * @return true if removed, false otherwise
	 */
	@Override
	public boolean remove(Object computer) {
		if (computer == null) {
			throw new NullPointerException("Computer cannot be null.");
		}
		Computer computerToRemove = (Computer) computer;
		return this.computers.remove(computerToRemove.getProductId(), computerToRemove);
	}

	/**
	 * Checks to see if hashmap contains computer collection
	 * @precondition null
	 * @postcondition none
	 * 
	 * @param computers the collection to check the hashmap for
	 * @return true if hashmap contains collection, false otherwise
	 */
	@Override
	public boolean containsAll(Collection<?> computers) {
		for (Object computer : computers) {
			if (computer == null) {
				throw new NullPointerException("Computer cannot be null");
			}
		}
		return this.computers.values().containsAll(computers);
	}

	/**
	 * Adds collection of computers to hashmap
	 * @precondition none
	 * @postcondition this.computers.size == 
	 * 				  this.computers.size@prev + computers.size()
	 * @param computers the computer collection to add
	 * @return true if added, false otherwise
	 */
	@Override
	public boolean addAll(Collection<? extends Computer> computers) {
		for (Computer computer : computers) {
			if (computer == null) {
				throw new NullPointerException("Cannot be null");
			}
		}

		boolean changed = false;
		for (Computer computer : computers) {
			if (this.add(computer)) {
				changed = true;
			}
		}
		return changed;
	}

	/**
	 * removes collection of computers to hashmap
	 * @precondition none
	 * @postcondition this.computers.size == 
	 * 				  this.computers.size@prev - computers.size()
	 * @param computers the computer collection to remove
	 * @return true if removed, false otherwise
	 */
	@Override
	public boolean removeAll(Collection<?> computers) {
		for (Object computer : computers) {
			if (computer == null) {
				throw new NullPointerException("Collection cannot contain a null value");
			}
		}
		boolean changed = false;
		for (Object computer : computers) {
			if (this.remove(computer)) {
				changed = true;
			}
		}
		return changed;
	}

	/**
	 * unsupported operation
	 */
	@Override
	public boolean retainAll(Collection<?> computers) {
		throw new UnsupportedOperationException();

	}

	/**
	 * Clears the collection
	 * @precondition none
	 * @postcondition this.computers.size() == 0
	 */
	@Override
	public void clear() {
		this.computers.clear();
	}
	
	/**
	 * Checks to see if hashmap contains a key
	 * @param key the key to search for
	 * @return true if contains, false otherwise
	 */
	public boolean containsKey(String key) {
		if (key.isEmpty() || key == null) {
			return false;
		}
		return this.computers.containsKey(key);
	}
	
	/**
	 * Gets a computer based on its product ID.
	 * @param key the computer's product ID.
	 * @return the computer with the corresponding product ID.
	 */
	public Computer get(String key) {
		return this.computers.get(key);
	}
	
	/**
	 * Puts a computer with its corresponding key in the collection.
	 * 
	 * @precondition key != null && !key.isEmpty() &&
	 * 				 computer != null 
	 * @param key the computer's product ID
	 * @param computer the computer to add
	 * @return the computer value that was added
	 */
	public Computer put(String key, Computer computer) {
		if (key == null || key.isEmpty()) {
			throw new IllegalArgumentException("Invalid product ID value.");
		}
		if (computer == null) {
			throw new IllegalArgumentException("Computer cannot be null");
		}
		return this.computers.put(key, computer);
	}
	
	/**
	 * Removes a computer based on its product ID.
	 * @precondition key != null && !key.isEmpty()
	 * @postcondition this.computers.size() ==
	 * 				  this.computers.size()@prev - 1
	 * @param key the computer's product ID
	 * @return the removed computer
	 */
	public Computer remove(String key) {
		if (key == null || key.isEmpty()) {
			throw new NullPointerException("Product ID cannot be null.");
		}
		return this.computers.remove(key);
	}
	
	/**
	 * Replaces a computer based on its product ID.
	 * @precondition key != null && !key.isEmpty()
	 * @postcondition this.computers.size() ==
	 * 				  this.computers.size()@prev - 1
	 * @param key the computer's product ID
	 * @param computer the computer to replace
	 * @return the replaced computer
	 */
	public Computer replace(String key, Computer computer) {
		if (key == null || key.isEmpty()) {
			throw new NullPointerException("Product ID cannot be null.");
		}
		if (computer == null) {
			throw new IllegalArgumentException("Computer cannot be null");
		}
		
		return this.computers.replace(key, computer);
	}
	
	/**
	 * Returns the values of the hashmap.
	 * @precondition none
	 * @postcondition none
	 * @return the computer values of the hashmap
	 */
	public Collection<Computer> values() {
		return this.computers.values();
	}
}
